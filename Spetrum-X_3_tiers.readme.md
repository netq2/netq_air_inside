Spectrum-X 3 Tiers with external NetQ

Demo Environment Access
The topology and NetQ are preconfigured with all the needed parameters to use this demo. You only need to connected nodes used in demo to any TS (standalone or opta)
Login the oob-mgmt-server using the default access credentials:

Username: ubuntu

Password: netq_123


Note: Once you first login, you must change the default password.
Once you logged in, run below command to attach nodes to any netq-appliance:

ubuntu@oob-mgmt-server:~$ ./scripts/configure_cl_agents.sh --cluster_server <master ip>,<worker1 ip>,<worker2 ip> --version 4.13  --reinstall


Switches crednetials: cumulus NvidiaR0cks!







