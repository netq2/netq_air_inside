#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Remove netq server telemetry configuration from switches containers
# Example: ./remove_netq_config_from_containers.sh
base_dir=/home/ubuntu/config_setup
ansible-playbook -i $base_dir/hosts $base_dir/remove_telemetry_config.yml -v
