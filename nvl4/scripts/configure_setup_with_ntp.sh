#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Configure setup with ntp
# Example: ./configure_ntp.sh --ntp_server 1.cumulusnetworks.pool.ntp.org
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
		-s|--ntp_server) image="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done
if [[ -z $ntp_server ]]; then ntp_server="1.cumulusnetworks.pool.ntp.org"; fi
base_dir=/home/ubuntu/config_setup
ansible-playbook -i $base_dir/hosts $base_dir/configure_nvl_switches.yml -v --extra-vars "ntp_server=$ntp_server"
ansible-playbook -i $base_dir/hosts $base_dir/configure_netq.yml -v --extra-vars "ntp_server=$ntp_server"

