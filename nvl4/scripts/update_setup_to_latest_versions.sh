#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Upate setup with latest versions
# Example: ./update_setup_to_latest.sh --update_netq true --update_switches false
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
		--update_netq) update_netq="$2"; shift ;;
		--update_switches) update_switches="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done
update_netq=${update_netq:-false}
update_switches=${update_switches:-true}
base_dir=/home/ubuntu/config_setup
if [[ $update_netq == true ]] ; then
	echo "Starting updating netq ..."
	echo "To follow up with progress login to netq-server and run tail -f /mnt/installables/run_log.log"
	ansible-playbook -i $base_dir/hosts $base_dir/install_latest_netq.yml -b --become-password-file=$base_dir/sudo_password.txt -v
fi
if [[ $update_switches == true ]] ; then
    echo "Copying telemetry-agent ..."
	sshpass -p '3tango' scp root@10.213.49.142:/auto/UFM/NetQ/agent/nvl4/telemetry-agent.img.gz /home/ubuntu/nvos_images/
	echo "Copying nvos_simulator ..."
	sshpass -p '3tango' scp root@10.213.49.142:/auto/UFM/NetQ/agent/nvl4/nvos_simulator.tar.gz /home/ubuntu/nvos_images/
	ansible-playbook -i $base_dir/hosts $base_dir/update_nvl4_switches_version.yml -v
	ansible-playbook -i $base_dir/hosts $base_dir/update_nvl4_l1_switches_version.yml -v
fi	

