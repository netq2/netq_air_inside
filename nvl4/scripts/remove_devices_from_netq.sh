#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Decomission devices from netq
# Example: ./remove_deviecs_from_netq.sh --nvlink_switches 60 --nvlink_gpus 40
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
		--nvlink_switches) nvlink_switches="$2"; shift ;;
		--nvlink_gpus) nvlink_gpus="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done
nvlink_switches=${nvlink_switches:-60}
nvlink_gpus=${nvlink_gpus:-40}
base_dir=/home/ubuntu/config_setup
ansible-playbook -i $base_dir/hosts $base_dir/remove_devices_via_cassandra.yml -v --extra-vars "nvlink_switches=$nvlink_switches nvlink_gpus=$nvlink_gpus"

