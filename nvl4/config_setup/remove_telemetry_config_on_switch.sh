#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Configure Remove netq configuration
# Read args
docker exec -t nvl4-telemetry-agent rm /etc/td-agent-bit/outputs.conf
docker exec -t nvl4-telemetry-agent rm /usr/share/netq/td-agent-bit/outputs.conf
docker exec -t nvl4-telemetry-agent supervisorctl restart all
