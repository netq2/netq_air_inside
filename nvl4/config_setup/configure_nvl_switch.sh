#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Configure NVL switch to with netq server
# Example: /auto/UFM/NetQ/scripts/configure_nvl_switch.sh --netq_server 10.176.232.12
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -s|--netq_server) netq_server="$2"; shift ;;
		-n|--ntp_server) image="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

docker exec -it nvl4-telemetry-agent hostname $(hostname)
if [[ -z $ntp_server ]]; then ntp_server="server 1.cumulusnetworks.pool.ntp.org"; fi

systemctl enable chronyd
firewall-cmd --permanent --add-service=ntp
firewall-cmd --reload
chronyd -q '$ntp_server iburst'
systemctl restart chronyd

if [[ -z $netq_server ]]; then
	docker exec -t nvl4-telemetry-agent /usr/bin/python3.9 /etc/netq/nvl4-http-handler.pyc '{"action": "add", "domain": "f", "domain_id": "1", "destinations": [{"tls": false, "ip_address": "'"$netq_server"'", "port": "30001"}]}'
fi
