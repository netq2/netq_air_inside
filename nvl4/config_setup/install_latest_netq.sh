#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Install Latest netq version

LOG_FILE="run_log.log"
NETQ_INSTALLATION_VER_FILE="/opt/scripts/netq_version.txt"

function log_command()
{
    echo "$(date +%F_%T) Running Command: $1" >> $LOG_FILE
}

function run_command()
{
        log_command "$1"
        $1 >> $LOG_FILE
}

read_netq_version(){
    while IFS== read key val;do
      if [ $key == "NETQ_VERSION" ];
          then
              NETQ_VERSION=`echo $val | sed 's/ *$//g'`   # Trim white spaces

      elif [ $key == "NETQ_AGENT_VERSION" ];
          then
            NETQ_AGENT_VERSION=`echo $val | sed 's/ *$//g'`
            NETQ_BASE_AGENT_VERSION=`echo $val | sed 's/ *.0$//g'`
      fi
  done < "$1"    # Input file
}

download_latest_agent(){
  # Params $1: agent_full_path, $2: agent_package_name_suffix
  latest_agent=`curl -s http://rohbuild03.mvlab.labs.mlnx/dev/deb/pool/netq-$NETQ_BASE_AGENT_VERSION/p/python-netq/ | grep -oE netq-agent_$NETQ_AGENT_VERSION-$2[^\"]*amd64.deb | tail -n -1`
  latest_apps=`curl -s http://rohbuild03.mvlab.labs.mlnx/dev/deb/pool/netq-$NETQ_BASE_AGENT_VERSION/p/python-netq/ | grep -oE netq-apps_$NETQ_AGENT_VERSION-$2[^\"]*amd64.deb | tail -n -1`
  run_command "wget http://rohbuild03.mvlab.labs.mlnx/dev/deb/pool/netq-$NETQ_BASE_AGENT_VERSION/p/python-netq/$latest_agent"
  run_command "wget http://rohbuild03.mvlab.labs.mlnx/dev/deb/pool/netq-$NETQ_BASE_AGENT_VERSION/p/python-netq/$latest_apps"
}

read_netq_version $NETQ_INSTALLATION_VER_FILE

if [[ "$NETQ_VERSION" ==  *"SNAPSHOT"* ]]; then
  is_release=false
  source=latest
else
  is_release=true
  source=release
fi

echo "#######################################################" >> $LOG_FILE
echo "Installing Latest Version..." >> $LOG_FILE

CMD='rm -rf /mnt/installables/*'
run_command "$CMD"

CMD="cd /mnt/installables/"
run_command "$CMD"

CMD="wget http://sm-telem-04.lab2300.labs.mlnx/netq-images/$source/NetQ-$NETQ_VERSION.tgz"
run_command "$CMD"
sleep 5

#### Getting latest NetQ Ubuntu agent (agent for netq servers) ####
download_latest_agent
#sleep 120
CMD="/usr/bin/apt -y install /mnt/installables/netq-apps*"
run_command "$CMD"
CMD='/usr/bin/apt -y install /mnt/installables/netq-agent*'
run_command "$CMD"

CMD='echo "netq-cli:
       opta-check: false
       override-memory-req-mb-cloud: 40000" > /etc/netq/netq.yml'
echo "netq-cli:
       opta-check: false
       override-memory-req-mb-cloud: 40000" > /etc/netq/netq.yml
log_command "$CMD"

CMD='netq config restart cli'
run_command "$CMD"

CMD='systemctl enable netqd'
run_command "$CMD"

CMD='systemctl start netqd'
run_command "$CMD"

CMD='netq bootstrap reset purge-db purge-images'
run_command "$CMD"
NETQ_ONPRIM_VERSION=`ls -l /mnt/installables/ -I "*opta*.tgz" | grep NetQ | awk '{print $9}'`
NETQ_OPTA_VERSION=`ls -l /mnt/installables/  | grep opta | awk '{print $9}'`

CMD="netq install standalone full interface eth0 bundle /mnt/installables/$NETQ_ONPRIM_VERSION"
run_command "$CMD"

