#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Install NVOS simulator on colossus machine
# Example: ./install_nvos_simulator_centos8.sh --netq_server 192.168.200.250 --telemetry_image /opt/telemetry-agent.img.gz --nvos_image /opt/nvos_simulator.tar.gz --system_type nvl4_switch
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -s|--netq_server) netq_server="$2"; shift ;;
        -i|--telemetry_image) image="$2"; shift ;;
        -t|--system_type) system_type="$2"; shift ;;
        -n|--nvos_image) nvos_image="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

LOG_DIR=/var/log/os_simulator
LOG_FILE=$LOG_DIR/console.log

##################### Install packages #####################
# Fix appsteams issues:
sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

# Install the required packages
dnf module install -y python27
dnf install -y tar
yum install -y python2-virtualenv

# Install docker
if [ -z "$(rpm -qa | grep docker)" ]; then
	yum install -y yum-utils
	yum-config-manager \
    		--add-repo \
    		https://download.docker.com/linux/centos/docker-ce.repo
	yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
fi
############################################################

# Remove old files
rm -rf /opt/mlnx/
rm -rf /opt/MSS/

# clone the simulation project
tar -xzf $nvos_image neo_simulation/
mv -f neo_simulation/onyx_simulator/* /opt/
rm -rf neo_simulation/

# Update system type
echo "SYSTEM_TYPE=$system_type" > /opt/mlnx/systype

cd /opt/mlnx/
python2 -m virtualenv venv
. /opt/mlnx/venv/bin/activate
pip install Flask
pip install flask_classy
pip install paramiko

# Fix the admin password
chmod +x /opt/mlnx/activate_shell.sh
rm -rf /usr/bin/activate_shell
cp /opt/mlnx/activate_shell.sh /usr/bin/activate_shell
if [ -z "$(cat /etc/passwd | grep activate_shell)" ]; then
	echo "admin:x:0:0:System Administrator:/root:/usr/bin/activate_shell" >> /etc/passwd
fi
if [ -z "$(cat /etc/shells | grep activate_shell)" ]; then
        echo "/usr/bin/activate_shell" >> /etc/shells
fi

echo "admin" | passwd --stdin admin

# Configure log rotate
mkdir -p $LOG_DIR
echo "$LOG_FILE {
        rotate 7
        daily
        compress
        delaycompress
        missingok
        notifempty
        create 660 linuxuser linuxuser
}" > /opt/MSS/logrotate
mv -f /opt/MSS/logrotate /etc/logrotate.d/os_simulator
chmod 644 /etc/logrotate.d/os_simulator
chown root.root /etc/logrotate.d/os_simulator

# Run and test the simulator
/opt/run_sim.sh
sleep 10
echo '{"cmd":"show system type"}' | curl -k -b ./cookie -X POST -d @- https://127.0.0.1/admin/launch?script=json

# Start docker service
service docker start
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

# Remove old nvlink
docker rm -f $(docker ps -f name=nvl4-telemetry-agent -q)
docker rmi -f $(docker images nvlink.img -q)

# Install and configure nvl4-telemetry-agent
docker load -i $image
docker run -dit --name nvl4-telemetry-agent --restart unless-stopped --network host -v /var/run:/var/run --privileged nvlink.img:latest
docker exec -t nvl4-telemetry-agent /usr/bin/python3.9 /etc/netq/nvl4-http-handler.pyc '{"action": "add", "domain": "testing", "domain_id": "1", "destinations": [{"ip_address": "'"$netq_server"'", "port": "30001", "tls": false}]}'
docker exec -t nvl4-telemetry-agent supervisorctl restart all

# Add startup services in case of system reboot
if [ -z "$(cat /etc/rc.d/rc.local | grep run_sim)" ]; then 
	echo "/opt/run_sim.sh; sleep 20; docker exec -t nvl4-telemetry-agent supervisorctl restart all" >> /etc/rc.d/rc.local; 
fi
chmod +x /etc/rc.d/rc.local
