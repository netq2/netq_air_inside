#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Run os simulator
LOG_DIR=/var/log/os_simulator
LOG_FILE=$LOG_DIR/console.log

# Activate the virtual environment
. /opt/mlnx/venv/bin/activate

# Run the simulator
pkill -9 python2;export PYTHONPATH=/opt/MSS;nohup python2 /opt/MSS/common/rest_driver.py >> $LOG_FILE 2>&1 &

