#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Enable https port in firewall
# Read args
firewall-cmd --zone=public --permanent --add-service=https; firewall-cmd --reload
