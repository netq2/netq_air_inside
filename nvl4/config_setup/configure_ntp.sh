#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Configure NTP on netq server
# Example: ./configure_ntp.sh --ntp_server 1.cumulusnetworks.pool.ntp.org
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
		-n|--ntp_server) image="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [[ -z $ntp_server ]]; then ntp_server="1.cumulusnetworks.pool.ntp.org"; fi

echo "netq_123" | sudo -S  apt install -y ntpdate
echo "netq_123" | sudo -S  ntpdate $ntp_server
