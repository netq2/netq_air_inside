#!/bin/bash
# @author:
#    Mazen Alkoa - mazenk@nvidia.com
#
# Decomisssion devices via cassandra
# Example: ./remove_devices_via_cassandra --nvlink_switches 60 --nvlink_gpus 40
# Read args
while [[ "$#" -gt 0 ]]; do
    case $1 in
		-s|--nvlink_switches) nvlink_switches="$2"; shift ;;
		-g|--nvlink_gpus) nvlink_gpus="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

nvlink_switches=${nvlink_switches:-60}
nvlink_gpus=${nvlink_gpus:-40}

for (( index=1; index <= $nvlink_switches; index++ )); do
	kubectl exec -it $(kubectl get pods | grep cassandra | awk '{print $1}') -- cqlsh -e "delete from netq.node where opid=0 and hostname='nvlink-switch$index';"
	kubectl exec -it $(kubectl get pods | grep cassandra | awk '{print $1}') -- cqlsh -e "delete from netq.inventory where opid=0 and hostname='nvlink-switch$index';"
done
for (( index=1; index <= $nvlink_gpus; index++ )); do
	kubectl exec -it $(kubectl get pods | grep cassandra | awk '{print $1}') -- cqlsh -e "delete from netq.node where opid=0 and hostname='nvlink-l1-switch$index';"
	kubectl exec -it $(kubectl get pods | grep cassandra | awk '{print $1}') -- cqlsh -e "delete from netq.inventory where opid=0 and hostname='nvlink-l1-switch$index';"
done
