<!-- AIR:tour -->

# NVIDIA NetQ 2es-NVUE-mini Topology

This environment demonstrates the NetQ 2es-nvue-mini
<!-- AIR:page -->

## Demo Environment Access

The topology and NetQ are preconfigured with all the needed parameters to use this demo. You only need to connected nodes used in demo to any TS(standalone or opta)

Login the `oob-mgmt-server` using the default access credentials:
 - Username: ***ubuntu***
 - Password: ***CumulusLinux!***

***Note:*** *Once you first login, you must change the default password.*

Once you logged in, run below command to attach nodes to any netq-appliance:
```bash
ubuntu@oob-mgmt-server:~$ ./configure_cl_agents.sh <TS_IP/OPTA_IP> <netq-version>
for eg: 
ubuntu@oob-mgmt-server:~$ ./configure_cl_agents.sh 10.188.45.92 4.8
```

To check out switch configuration, make changes or use NetQ CLI, you can access the switches via the `oob-mgmt-server` jump server,  the web integrated console or create an SSH service to access the `oob-mgmt-server` using any SSH client.  
For more information and step-by-step instructions, check out NVIDIA Air [Quick Start](https://docs.nvidia.com/networking-ethernet-software/guides/nvidia-air/Quick-Start/) guide.

**Note:** Make sure that the `Advanced view` toggle is enabled on the top-right corner of the screen.

Once you login to the `oob-mgmt-server`, you can access any device in the topology using its hostname.

Login to the servers using just the hostname - `ssh <hostname>` (all have the same username - `ubuntu`, which is identical to the `oob-mgmt-server` username).

Default server credentials are:  
 - Username: ***ubuntu***
 - Password: ***nvidia*** 

```bash
ubuntu@oob-mgmt-server:~$ ssh tor-1
```
Login to the switches using `cumulus` username - `ssh cumulus@<hostname>`. To ease the access, `cumulus` username was set to passwordless authentication. 

```bash
ubuntu@oob-mgmt-server:~$ ssh cumulus@tor-1
```

<!-- AIR:page -->

## Demo Topology Information 

The topology consists of 5 switches and 3 hosts which is replication 2es_nvue_mini. 

Hostname          Status           NTP Sync Version                              Sys Uptime                Agent Uptime              Reinitialize Time          Last Changed
----------------- ---------------- -------- ------------------------------------ ------------------------- ------------------------- -------------------------- -------------------------
hostd-11          Fresh            yes      4.8.0-ub20.04u44~1699074936.80e66493 Tue Nov 14 10:50:44 2023  Tue Nov 14 12:29:53 2023  Fri Nov 24 06:02:25 2023   Fri Nov 24 06:21:13 2023
                                            7
hosts-11          Fresh            yes      4.8.0-ub20.04u44~1699074936.80e66493 Tue Nov 14 10:52:10 2023  Tue Nov 14 12:31:07 2023  Fri Nov 24 06:02:28 2023   Fri Nov 24 06:21:14 2023
                                            7
hosts-12          Fresh            yes      4.8.0-ub20.04u44~1699074936.80e66493 Tue Nov 14 10:52:10 2023  Tue Nov 14 12:31:12 2023  Fri Nov 24 06:02:32 2023   Fri Nov 24 06:21:20 2023
                                            7
spine-1           Fresh            yes      4.8.0-cl4u44~1699245971.f796c0644    Tue Nov 14 10:52:16 2023  Tue Nov 14 12:17:31 2023  Fri Nov 24 06:02:30 2023   Fri Nov 24 06:21:32 2023
spine-2           Fresh            yes      4.8.0-cl4u44~1699245971.f796c0644    Tue Nov 14 10:50:50 2023  Tue Nov 14 12:16:14 2023  Fri Nov 24 06:02:31 2023   Fri Nov 24 06:21:28 2023
tor-1             Fresh            yes      4.8.0-cl4u44~1699245971.f796c0644    Tue Nov 14 10:52:16 2023  Tue Nov 14 12:17:47 2023  Fri Nov 24 06:02:32 2023   Fri Nov 24 06:21:28 2023
torc-11           Fresh            yes      4.8.0-cl4u44~1699245971.f796c0644    Tue Nov 14 10:52:16 2023  Tue Nov 14 12:17:49 2023  Fri Nov 24 06:02:32 2023   Fri Nov 24 06:21:35 2023
torc-12           Fresh            yes      4.8.0-cl4u44~1699245971.f796c0644    Tue Nov 14 10:52:16 2023  Tue Nov 14 12:17:48 2023  Fri Nov 24 06:02:32 2023   Fri Nov 24 06:21:34 2023



<!-- AIR:tour -->
?cache=13
