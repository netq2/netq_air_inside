#!/bin/bash
NETQ_TS=$1 #10.176.235.12
if [[ -z $NETQ_TS ]];
then
    echo " Missing mandatory arguments: NETQ_TS"
    exit 1
fi
rm -rf netq*
all_agents_url=http://rohbuild03.mvlab.labs.mlnx/dev/deb/pool/netq-latest/p/python-netq/
netq_cl_apps_latest_file=$(curl -s $all_agents_url | grep -o 'netq-apps_[^"]*amd64.deb' | grep 'cl4' | uniq)
netq_cl_apps_latest_full_path=$all_agents_url$netq_cl_apps_latest_file
sudo wget $netq_cl_apps_latest_full_path
netq_cl_agent_latest_file=$(curl -s $all_agents_url | grep -o 'netq-agent_[^"]*amd64.deb' | grep 'cl4' | uniq)
netq_cl_agent_latest_full_path=$all_agents_url$netq_cl_agent_latest_file
sudo wget $netq_cl_agent_latest_full_path

switch_hostnames=($(awk '$1 ~ /^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/ && $2 ~ /^[a-zA-Z0-9-]+$/ && $2 !~ /localhost|server|oob-mgmt/ {print $2}' /etc/hosts))

process_host() {
    hostname=$1
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo rm -rf netq*"
    sleep 3
    sshpass -p CumulusLinux! scp -o StrictHostKeyChecking=no /home/ubuntu/netq-agent* cumulus@$hostname:/home/cumulus
    sshpass -p CumulusLinux! scp -o StrictHostKeyChecking=no /home/ubuntu/netq-apps* cumulus@$hostname:/home/cumulus
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo dpkg -i netq-agent*"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo dpkg -i netq-apps*"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "echo 'SystemMaxUse=100M' | sudo tee -a /etc/systemd/journald.conf && sudo systemctl restart systemd-journald"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo rm -rf /run/log/journal/*"
    sleep 5
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo systemctl enable netqd"
    sleep 10
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo systemctl start netqd"
    sleep 10
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo netq config add agent server $NETQ_TS vrf mgmt"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo netq config restart agent"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo netq config add cli server $NETQ_TS vrf mgmt"
    sshpass -p CumulusLinux! ssh -o StrictHostKeyChecking=no  cumulus@$hostname "sudo netq config restart cli"

}
# Counter for background jobs
counter=0

# Max number of concurrent background jobs
max_jobs=10

for hostname in "${switch_hostnames[@]}"; do

    process_host $hostname &
        ((counter++))
        # If counter reaches max_jobs, wait for all background jobs to complete
    if [[ $counter -eq $max_jobs ]]; then
        wait
        counter=0
    fi
done

wait