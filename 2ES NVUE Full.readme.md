NVIDIA NetQ 2ES Full Topology
This environment demonstrates the NetQ 2ES Full


Demo Environment Access
The topology and NetQ are preconfigured with all the needed parameters to use this demo. You only need to connected nodes used in demo to any TS(standalone or opta)
Login the oob-mgmt-server using the default access credentials:

Username: ubuntu

Password: CumulusLinux!


Note: Once you first login, you must change the default password.
Once you logged in, run below command to attach nodes to any netq-appliance:

ubuntu@oob-mgmt-server:~$ ./configure_cl_agents.sh <TS_IP/OPTA_IP> <netq-version>
for eg: 
ubuntu@oob-mgmt-server:~$ ./configure_cl_agents.sh 10.188.45.92 4.8


To check out switch configuration, make changes or use NetQ CLI, you can access the switches via the oob-mgmt-server jump server,  the web integrated console or create an SSH service to access the oob-mgmt-server using any SSH client.
For more information and step-by-step instructions, check out NVIDIA Air Quick Start guide.
Note: Make sure that the Advanced view toggle is enabled on the top-right corner of the screen.
Once you login to the oob-mgmt-server, you can access any device in the topology using its hostname.
Login to the servers using just the hostname - ssh <hostname> (all have the same username - ubuntu, which is identical to the oob-mgmt-server username).
Default server credentials are:

Username: ubuntu

Password: nvidia



ubuntu@oob-mgmt-server:~$ ssh tor-1


Login to the switches using cumulus username - ssh cumulus@<hostname>. To ease the access, cumulus username was set to passwordless authentication.

ubuntu@oob-mgmt-server:~$ ssh cumulus@tor-1





